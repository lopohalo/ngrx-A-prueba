import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { GeneralModuleRoutingModule } from "./general-module-routing.module";
import { BasegeneralComponent } from "./basegeneral/basegeneral.component";

export const modals = [];

@NgModule({
  declarations: [BasegeneralComponent],
  imports: [CommonModule, GeneralModuleRoutingModule],
  exports: [],
  entryComponents: [],
  providers: [],
})
export class GeneralModuleModule {}
